import Head from 'next/head'
import clientPromise from '../lib/mongodb'

import { listUsuarios } from "../bo/usuarios"

export default function Home({ isConnected, usuarios }) {
  let id = 0
  return (
    <div className="container">
      <Head>
        <title>Ache Músicos</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>Ache Músicos</h1>

        {!isConnected && (
          <h2 className="subtitle">
            Você não está conectado ao servidor. Tente mais tarde.
          </h2>
        )}

        <h3>Usuários:</h3>
        <ul>
          {usuarios.map(usuario => (
            <li key={id++}>
              <h4>{usuario.nome}</h4>
              <div>{usuario.email}</div>
              <h5>Papéis:</h5>
              <ul>
                {usuario.papeis.map(p => (
                  <li key={id++}>
                    <h6>{p.nome}</h6>
                    <h7>Habilidades:</h7>
                    <ul>
                      {p.habilidades.map(h => (
                        <li key={id++}>{h.nome} {h.nivel} ({h.anos} anos)</li>
                      ))}
                    </ul>
                  </li>
                ))}
              </ul>
            </li>
          ))}
        </ul>
      </main>
    </div>
  )
}

export async function getServerSideProps() {
  try {
    await clientPromise

    const usuarios = await listUsuarios()

    return {
      props: {
        isConnected: true,
        usuarios: JSON.parse(JSON.stringify(usuarios))
      },
    }
  } catch (e) {
    console.error(e)
    return {
      props: { isConnected: false, usuarios: [] },
    }
  }
}
