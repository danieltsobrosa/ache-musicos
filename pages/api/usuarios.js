import { listUsuarios } from "../../bo/usuarios"

async function handler(req, res) {
  res.json(await listUsuarios())
}

export default handler