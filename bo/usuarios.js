import { connectToDatabase } from "../util/mongodb"

export async function listUsuarios({
    limit = 20,
    orderByField = 'cadastro',
    orderByDirection = -1
} = {}) {
  const { db } = await connectToDatabase()

  const usuarios = await db
    .collection("usuarios")
    .find({})
    .sort({ [orderByField]: orderByDirection })
    .limit(limit)
    .toArray()

  return usuarios
}